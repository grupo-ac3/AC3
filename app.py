from flask import Flask, request

app = Flask(__name__)

produtos = [
    {
        "id":1,
        "nome":"teclado",
        "preco": 100
    }
]


@app.route('/produtos')
def TodosProdutos():
    return produtos, 200


@app.route('/produtos/cadastrar', methods=["POST"])
def cadastrarProduto():
    try:
        nome = request.args.get("nome")
        preco = request.args.get("preco")
        if nome and preco:
            if len(produtos) > 0:
                id = produtos[-1]["id"] + 1
            else:
                id = 1
            produtos.append({"id": id, "nome": nome, "preco": preco})
            return "produto inserido com sucesso", 201
        else:
            raise ValueError
    except:
        return "produto não inserido", 404


@app.route("/produtos/deletar", methods=["DELETE"])
def deletarProduto():
    id = request.args.get("id")
    for produto in produtos:
        print(produto)
        if produto["id"] == int(id):
            print("entrou no if")
            produtos.remove(produto)
            return f"Produto com id {id} foi deletado", 200
    return f"Produto com id {id} não foi encontrado", 404


if __name__ == "__main__":
    app.run(debug=True)
