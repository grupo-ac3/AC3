import unittest
from app import app

class AppTest(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
    
    def testeRotaProdutos(self):
        response = self.app.get('/produtos')
        self.assertEqual(response.status_code, 200, 'Não deu certo não pivete')

    def testeRotaCadatrar(self):
        response = self.app.post('/produtos/cadastrar?nome=produtoTeste&preco=100')
        self.assertEqual(response.text, 'produto inserido com sucesso')
        self.assertEqual(response.status_code, 201)
    
    def testeRotaCadastrarErrado(self):
        response = self.app.post('/produtos/cadastrar')
        self.assertEqual(response.status_code, 404)


if __name__ == "__main__":
    unittest.main()
